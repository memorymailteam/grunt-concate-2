
module.exports = function(grunt) {
// Project configuration.
grunt.initConfig({
  concat: {
    basic: {
      src: ['src/*.js'],
      dest: 'dist/basic.js',
    },
    extras: {
      src: ['src/*.js'],
      dest: 'dist/with_extras.js',
    },
  },
});

 // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-concat');

  // Default task(s).
  grunt.registerTask('default', ['concat']);

};
